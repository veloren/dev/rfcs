- Feature Name: Death and Respawn System
- Start Date: 2018-07-15
- RFC PR: 
- Rust Issue: 

# Summary
[summary]: #summary

A death and respawn system. In this system, when you die, you lose your soul (which impacts your HP) and some of your money. You can either go back to where you died to retrieve your soul and money, or you can go back to town to fix your soul.

# Motivation
[motivation]: #motivation

There needs to be some sort of punishment when you die. Allowing the player to infinitely respawn without punishment is not engaging, and leaves the player's character with no meaning. This is a system that is punishing enough to make the player think about what they're doing, while soft enough that the player won't rage at every death in the game.

# Guide-level explanation
[guide-level-explanation]: #guide-level-explanation

When you die, you lose half of the currency your character is carrying and respawn at the furthest point on the current chunk from your death site. Your character's "soul" is broken, so your health bar appears cracked and you cannot heal above 80% of your HP.

From here, you (the player) have two options:
- Go back to town, and pay a priest/shaman/guru/NPC to fix your soul.
- Go back to where you died, where they can retrieve their soul and pick up your lost cash.

If you go back to town, you spend a set amount of money to get your soul "fixed". When your soul is fixed, your health bar's non-cracked appearance is restored, your HP is no longer limited, and your HP is fully restored. You do not get your money back. It may stay at your death site forever, or it may disappear; this is up for discussion.

If you go back to where you died, your death site will have three things around it:
- A gravestone, marking where you died,
- A spirit in the form of your character floating behind the gravestone,
- Your money on the floor in front of the gravestone.
You can go to your soul and press the interact key to retrieve it. When you retrive your soul, there is an animation of your character absorbing the soul, your health bar's non-cracked appearance is restored, your HP is no longer limited, and your HP is fully restored. You can pick up the money laying on the ground as normal at any time.

If you are killed while your soul is broken, you will respawn again at the furthest point on the current chunk and lose another half of your money, but will not lose an extra percentage of your health.

# Reference-level explanation
[reference-level-explanation]: #reference-level-explanation

See [guide-level-explanation].

# Drawbacks
[drawbacks]: #drawbacks

Players may be annoyed at having to recover lost money, or at a permanent loss of money if they die in a hard area.

# Rationale and alternatives
[alternatives]: #alternatives

This approach to death allows the player to retrieve their items at little punishment for silly deaths, like falling off of a cliff or forgetting to watch your health against weak enemies, while also making sure the player reconsiders going back into a situation they should not have been in in the first place, like going into an overleveled dungeon or fighting a boss that is too hard for them.
- The loss of money provides the player with a minor punishment for silly deaths (they only have to walk back)
- The loss of maximum health forces a player to reconsider their approach to a difficult obstacle and whether or not they should attempt it again
- The combination of the two provides a soft punishment for all deaths, as well as a prod in the right direction for players that are trying to complete challenges that they cannot complete yet.

An alternative would be only a loss of money, and no loss to HP. With this mechanic, a close fight can still be won by going back and fighting again, but players may feel encouraged to continuously go back to a dangerous area and throw themselves to their death or near-death to get their money back, instead of realizing that they have little chance of survival.

# Prior art
[prior-art]: #prior-art

Dark Souls 2 has a mechanic where every time you die, you lose a portion of your health. However, this idea only takes a portion of your health on the first death; Dark Souls 2 stacks the effect.

When you die in Hollow Knight, soul icon in the top left appears cracked, you lose all of your money, and your soul remains where you died. To retrieve your money, you go back and hit your soul twice. This idea uses the idea of a cracked soul, but does not make you lose all of your money nor does it force you to fight your soul to get it back.

In Terraria softcore mode, you lose half of your money when you die. In mediumcore, you lose some items and all of your money.

# Unresolved questions
[unresolved]: #unresolved-questions

There are two main questions in this design proposal:
- Does your money disappear if you return to town instead of retrieving your soul?
- Do you respawn with a portion of your max HP, or is that too punishing?



4/18/2021 rough consensus, Sam/slipped/Pfau (vlaues uncertain but based on current death rates which are agreed too high, lower death rates would allow higher penalties)

- coin loss on death (5% of what is currently carried) <-- becomes more interesting when we start dropping coins in dungeons
- durability loss of armor on death (5% durability, first stat loss at 50%, no stats at 0%)
- max health loss leaving you with minimum 60% health?
- corpse retrieval to restore health and cash, durability loss stays
- choice to return to town on death with full health but a greater coin and durability penalty, soul can't be retrieved anymore
- NPCs using an Anvil can repair damaged items, costing a certain amount per point restored
- Higher quality items have more durability points and are therefore more expensive to maintain
