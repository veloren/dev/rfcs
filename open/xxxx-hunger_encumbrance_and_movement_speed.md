- Feature Name: weight_speed_and_health_consumables
- Start Date: 2021-10-16
- RFC Number: (leave this empty for now - the core developers will give you a number to put here when the RFC is submitted)
- Tracking Issue: (leave this empty - the core developers will give you a number to put here when the RFC is submitted)

# Summary
[summary]: #summary

Add a hunger stat that decreases player speed and carrying strength. Food consumables would primarily be used to decrease hunger (though they could still provide health and other buffs). Carrying strength would only apply to items worn or in the hands of the character (not items in the inventory). Hunger would *not* cause damage to the player, only limit their maneuverability and the weight of armor and weapons they can carry.

# Motivation
[motivation]: #motivation

Many players ignore food items and only use potions as potions increase health faster. This RFC proposes a method to provide utility to food items that is useful despite the presence of potions.
Currently there are no economic sinks in the game. Encouraging players to top up on food provides a sink and also provides an explanation for why villagers are producing crops (the village guards theoretically protect them from outside violent threat). While some players dislike the idea of constantly maintaining various stats many games employ this very thing as the only game mechanic demonstrating that it does have an appeal.

# Guide-level explanation
[guide-level-explanation]: #guide-level-explanation

Items each have a set weight. Characters have an encumbrance stat that is directly proportional to the total weight of worn armor and carried weapons and inversely proportional to the characters hunger (alternatively could be described as directly proportional to satiation/fullness). The weight of items in the inventory but not worn/carried in the hand do not contribute to encumbrance. Increased encumbrance reduces player movement speed (and potentially weapon buildup and recovery durations?).

Hunger can be decreased by consuming food. With no hunger a character would feel little or no effect from item weight. As hunger increases the effect of weight becomes increasingly apparent.

Potions are useful for increasing health. Food now has a separate unique use. "Potions of Fullness" may be added later.

Key take aways: Food primarily decreases hunger (and may also give a health buff), (current health) potions primarily increase health

Weight may also affect swimming?

# Reference-level explanation
[reference-level-explanation]: #reference-level-explanation

Note: Stamina will be referred to as `Energy` or energy in the following text to be consistent with the game code.
Hunger (or fullness, but I find that a confusing term as it can apply to other things than hunger satiation) would be implemented as a character stat in a similar fashion to `Health`, `Poise`, and `Energy`. Hunger would increase using a similar acceleration scheme as `Energy`. Food would apply a short buff that decreases hunger in a similar manner to how potions currently work with health. Hunger would range from 0 to 100 or 0.0 to 1.0 and be scaled by a factor of 256 for the reasons noted in the comments for the `Health` and `Energy` components.

Weight would be added as a stat to items by adding a `weight` field to `ItemDef` where `weight` would be an `Option<f32>` with units of Veloren kilograms or `Option<u32>` with units of Veloren grams. Items with a `weight` value of `None` would have no weight. Encumbrance would be calculated from the total weight of equipped items and hunger. The exact formulation has not been determined yet and would likely need testing and tweaking in-game. Here are some potential options:

Encumbrance = k * total_weight / (1.0 - if hunger == 1 { 0.99 } else { hunger })       where `k` is some constant
Encumbrance = k * hunger * total_weight^(1.0 + hunger)
Encumbrance = k * hunger * (k2 * hunger + total_weight).powi(2)

Encumbrance would be calculated from the inventory in a similar manner to the `combat_rating()` function in `common/src/combat.rs`.

Encumbrance would then factor into efficiency in the `basic_move()` function in `common/src/states/util.rs` by altering the `move_speed_modifier` of the `Stats` component.
```rs
 /// Updates components to move player as if theyre on ground or in air
 297 fn basic_move(data: &JoinData<'_>, update: &mut StateUpdate, efficiency: f32) {
 298     let efficiency = efficiency * data.stats.move_speed_modifier * data.stats.friction_modifier;
 299 
 300     let accel = if data.physics.on_ground.is_some() {
 301         data.body.base_accel()
 302     } else {
 303         data.body.air_accel()
 304     } * efficiency;
 305
 306    ...
```

Here is an example of how the `move_speed_modifier` is currently modified by a buff (in `common/systems/src/buff.rs`):

```rs
190                     for effect in &mut buff.effects {
191                         match effect {
                            ...
298                             BuffEffect::MovementSpeed(speed) => {
299                                 stat.move_speed_modifier *= *speed;
300                             },
```

# Drawbacks
[drawbacks]: #drawbacks

- Adds micromanagement task to character wellbeing. Many players do not like these kinds of mechanics.
- Adds timed "damage" (not health damage of course) of some kind decreasing player invulnerability. Many players enjoy video games for the power trip and dislike changes that make their characters more vulnerable.
- Item weight can be confusing for UX.

# Rationale and alternatives
[alternatives]: #alternatives

Most games with hunger mechanics will deal damage when a character's hunger meter is empty. This kind of micromanagement can be stressful and take away from other parts of the game. Since Veloren tends to be more action oriented than survival oriented (at least for the time being) it does not make too much sense to tie hunger to damage.

Instead of encumbrance, food could increase maximum health temporarily. This mechanic would need to be limited to a set quantity of food items being used at any given time. It would likely cause players to ignore "lower tier" food like cheese and apples and only use mushroom curry and the like.

# Prior art
[prior-art]: #prior-art

Valheim uses a food system where food increases your maximum health. The previous section on alternatives explains why this mechanic is not the best fit for Veloren.
Minecraft has a hunger meter. When your hunger bar is empty, you take damage. How full or empty the hunger bar is does not matter provided it is not empty. This has the damage problem and even if we did adopt a similar system, it would not provide a separate utility for food from potions.
Dwarf fortress has hunger but I do not know the exact mechanics. I would assume increased hunger means less stamina, strength, and speed. Hunger also leads to death and as mentioned before, hunger causing death is a highly contentions topic in Veloren and intentionally not approached directly in this RFC.

# Unresolved questions
[unresolved]: #unresolved-questions

- What happens to a character's hunger when a player logs off? Does it get saved to the database? (Probably yes)
- Do new characters start with a full belly? (Probably yes)
- How would food spoilage (separate feature) play with this?
